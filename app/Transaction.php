<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    public function report()
    {
    	return $this->hasOne('App\Report');
    }

    public function insertNewTransaction($report_id,$reference_code,$actual_amount)
    {
    	$payout_amount = $actual_amount - ($actual_amount * 0.20);

    	if(Auth::check()){
    		$user_id = Auth::user()->id;
    		$isCompany = 0;
    		$transaction_type = 'Payout';
    		$status = 'Claimed';
    	}else{
    		$user_id = Auth::guard('companies')->user()->id;
    		$isCompany = 1;
    		$transaction_type = 'Payment';
    		$status = 'Pending';
    	}
    	
    	$transaction = new Transaction();
    	$transaction->report_id = $report_id;
    	$transaction->user_id = $user_id;
    	$transaction->is_company = $isCompany;
    	$transaction->reference_code = $reference_code;
    	$transaction->transaction_type = $transaction_type;
    	$transaction->actual_amount = $actual_amount;
    	$transaction->payout_amount = $payout_amount;
    	$transaction->status = $status;
    	$transaction->save();

    	return 'okay';

    }
}
