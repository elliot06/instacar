<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImpactRange extends Model
{
    protected $table = 'category_range';

    public function company()
    {
      return $this->belongsTo('App\Company');
    }

    public function policy()
    {
    	return $this->belongsTo('App\CompanyPolicy');
    }
}
