<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DuplicateReport extends Model
{
    //
    protected $table='duplicate_report_info';


    public function hacker()
    {
        return $this->hasMany('App\Hacker');
    }

    public function profile()
    {
        return $this->hasOne('App\Profile','hacker_id');
    }

}
