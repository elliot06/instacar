<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportImage extends Model
{
    protected $table = 'report_images';

    public function report()
    {
    	return $this->belongsToOne('App\Report');
    }
}
