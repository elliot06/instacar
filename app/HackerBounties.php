<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Hacker;
use App\Company;
use App\Report;

class HackerBounties extends Model
{

    protected $table='hacker_bounties';
    
    public function hacker()
    {
    	return $this->belongsTo('App\Hacker');
    }
    public function report()
    {
    	return $this->belongsTo('App\Report');
    }
    public function company()
    {
    	return $this->belongsTo('App\Company');
    }
}
