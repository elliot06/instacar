<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BountyRange extends Model
{
    protected $table = 'bounty_range';

    public function vulnerability()
    {
    	return $this->belongsTo('App\Vulnerability');
    }
}

