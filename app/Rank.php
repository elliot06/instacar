<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Rank extends Model
{
    //
    protected $table = 'ranks';

    public function profile()
    {
    	return $this->hasMany('App\Profile','rank_id','id');
    }

    public function getTopTenUsers()
    {
    	$users = DB::table('perma_hacker_acc')->join('hacker_profile', 'perma_hacker_acc.id', '=', 'hacker_profile.hacker_id')->join('ranks', 'hacker_profile.rank_id', '=', 'ranks.id')->select('perma_hacker_acc.username', 'hacker_profile.*', 'ranks.rank_name','ranks.rank_pic')->orderBy('hacker_profile.rp', 'desc')
            ->Paginate(10);
        if($users !== null){
        	return $users;
    	}else{
    		return null;
    	}
    }
 
}
