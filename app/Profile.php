<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
	protected $table = 'hacker_profile';

    public $fillable = ['login_streak','rp'];

    public function hacker()
    {
    	return $this->belongsTo('App\Hacker');
    }

    public function rank()
    {
    	return $this->belongsTo('App\Rank');
    }

    public function chat()
    {
        return $this->belongsTo('App\Chat');
    }
}
