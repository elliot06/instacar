<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Report;

class BillingInformation extends Model
{
    //
     protected $table = 'billing_information';


     public function report()
     {
     	return $this->belongsToMany('App\Report');
     }

     public function package()
     {
     	return $this->belongsToMany('App\Package');
     }

     
}
