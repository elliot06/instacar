<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDeposit extends Model
{
    protected $table = 'hacker_bank';

   public function hacker()
   {
   		return $this->belongsTo('App\Hacker');
   }
}
