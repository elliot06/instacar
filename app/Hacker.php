<?php

namespace App;

use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\HackerBounties;
use Carbon\Carbon;
use DB;
use Auth;

class Hacker extends Model implements AuthenticatableContract,
CanResetPasswordContract {
    use Authenticatable, CanResetPassword;


    protected $table = 'perma_hacker_acc';
    protected $dates = ['created_at','updated_at','last_activity'];

    public function profile()
    {
        return $this->hasOne('App\Profile','hacker_id');
    }

    public function chat()
    {
        return $this->hasMany('App\Chat');
    }


    public function report()
    {
        return $this->hasMany('App\Report');
    }

    public function report_comment()
    {
        return $this->hasMany('App\ReportComment');
    }

    public function post()
    {
        return $this->hasMany('App\Post');
    }
    public function hacker_bounties()
    {
        return $this->hasMany('App\HackerBounties','hacker_id');
    }

    public function paypal()
    {
        return $this->hasOne('App\Paypal','hacker_id');
    }

    public function bank()
    {
        return $this->hasOne('App\BankDeposit','hacker_id');
    }
    //---------------Friendship Functions------------------

    public function getName()
    {
        if($this->first_name && $this->last_name)
        {
            return "{$this->first_name} {$this->last_name}";
        }
        if($this->first_name)
        {
            return $this->first_name;
        }
        return null;
    }
    public function getUsername()
    {
        return $this->username;
    }
    public function getNameOrUsername()
    {
        return $this->getName() ?: $this->username;
    }
    public function getFirstNameOrUsername()
    {
        return $this->first_name ?: $this->username;
    }
    public function friendsOfMine()
    {
        return $this->belongsToMany('App\Hacker', 'hacker_friends', 'user_id', 'friend_id');
    }
    public function friendOf()
    {
        return $this->belongsToMany('App\Hacker', 'hacker_friends', 'friend_id', 'user_id');
    }
    public function friends()
    {
        return $this->friendsOfMine()->wherePivot('accepted', true)->get()->merge($this->friendOf()->wherePivot('accepted', true)->get());
    }
    public function friendRequests()
    {
        return $this->friendsOfMine()->wherePivot('accepted', false)->get();
    }
    public function friendRequestsPending()
    {
        return $this->friendOf()->wherePivot('accepted', false)->get();
    }
    public function hasFriendRequestPending(Hacker $user)
    {
        return (bool) $this->friendRequestsPending()->where('id', $user->id)->count();
    }
    public function hasFriendRequestReceived(Hacker $user)
    {
        return (bool) $this->friendRequests()->where('id', $user->id)->count();
    }
    public function addFriend(Hacker $user)
    {
        $this->friendOf()->attach($user->id);
    }
    public function deleteFriend(Hacker $user)
    {
        $this->friendOf()->detach($user->id);
        $this->friendsOfMine()->detach($user->id);
    }
    public function ignoreFriend(Hacker $user)
    {
        $this->friendsOfMine()->detach($user->id);
        

    }
    public function cancelFriend(Hacker $user)
    {
        $this->friendsOfMine()->detach($user->id);
        $this->friendOf()->detach($user->id);

    }
    public function acceptFriendRequest(Hacker $user)
    {
        $this->friendRequests()->where('id', $user->id)->first()->pivot->update([
                                                                                'accepted' => true,
                                                                                ]);
    }
    public function isFriendsWith(Hacker $user)
    {
        return $this->friends()->where('id', $user->id)->count();
    }

    public function countRequests()
    {
        return $this->friendRequests()->count();

    }
    //-------------End of Friendship Functions-----------------------

    public static function modifyLoginStreak()
    {
        $hacker = Hacker::find(Auth::user()->id);
        $hacker_profile = $hacker->profile;

        $creation_date = $hacker->created_at;
        $old_date = $hacker->last_activity->addDays(1);
        $new_date = $hacker->last_activity;
        $login_streak = $hacker->profile->login_streak;


        if($old_date != $creation_date){
            if($old_date == $new_date && $login_streak < 10){
                $hacker_profile->update(['login_streak' => $login_streak + 1]);
                if ($hacker->profile->login_streak == 10) {
                    $hacker_profile->update(['login_streak' => 0,'rp' => $hacker_profile->rp +1]);
                    return "add_rp";
                }
                return "streak";
            }elseif($login_streak == 10) {
                $hacker_profile->update(['login_streak' => 0,'rp' => $hacker_profile->rp +1]);
                return "add_rp";
            }elseif($old_date > $new_date){
                return "none";
            }elseif ($old_date < $new_date) {
                $hacker_profile->update(['login_streak' => 0]);
                return "reset_rp";
            }
        }else{
            return "none";
        }
    }

    
}
