<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyDomain extends Model
{
    protected $table = 'company_domain';

    public function company()
    {
      return $this->belongsTo('App\Company');
    }

    public function policy()
    {
    	return $this->belongsTo('App\CompanyPolicy');
    }
}
