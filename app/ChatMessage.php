<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Hacker;

class ChatMessage extends Model
{
    //

    protected $table='chat_messages';
    protected $dates = ['read_at'];

    public function chat(){
        return $this->hasOne('App\Chat');
    }

    public function hacker(){
        return $this->belongsTo('App\Hacker');
    }
}
