<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyImpactRange extends Model
{
    protected $table = 'company_category_range';

    public function company()
    {
    	return $this->belongsTo('App\Company');
    }
}
