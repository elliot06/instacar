<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Report;

class Notification extends Model
{
    protected $table = 'notifications';

    public static function showNotifications($report_id,$user_id,$is_company)
    {
    	return DB::table('notifications')->where(['user_id' => $user_id, 'is_company' => $is_company])->orderBy('created_at','DESC')->get();
    	
    }

    public static function showUnread($user_id,$is_company)
    {	
    	return DB::table('notifications')->where(['user_id' => $user_id, 'is_company' => $is_company, 'is_unread' => 0])->orderBy('created_at','DESC')->get();	
    }

}