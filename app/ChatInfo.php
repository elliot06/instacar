<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatInfo extends Model
{
    //
    protected $table='chat_information';


    public function hacker()
    {
        return $this->hasMany('App\Hacker');
    }

    public function profile()
    {
        return $this->hasOne('App\Profile','hacker_id');
    }

}
