<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    public function hacker()
    {
    	return $this->belongsTo('App\Hacker');
    }

    public function profile()
    {
    	return $this->belongsTo('App\Profile');
    }
}
