<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Vulnerability;

class Package extends Model
{
    //
    protected $table='packages';

    public function vulnerability()
    {
    	return $this->hasOne('App\Vulnerability','package_id');
    }
}
