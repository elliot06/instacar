<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $table = 'messages';

    public function report()
    {
    	return $this->belongsTo('App\Report');
    }

   
    public function participant()
    {
        return $this->hasOne('App\Participant','user_id');
    }

}
