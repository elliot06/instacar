<?php

namespace App\Http\Controllers;

use App\Subscriptions;
use App\Company;
use App\CompanyPolicy;
use App\CompanyProfile;
use App\CompanyBounty;
use App\CompanyDomain;
use App\Vulnerability;
use App\PackageAmount;
use App\Report;
use App\Message;
use App\Package;
use App\BillingInformation;
use App\CompanyPayment;
use App\HackerBounties;
use App\BountyRange;
use App\Notification;
use Hash;
use Response;
use BrowserDetect;
use Mail;
use Session;
use PDF;
use Storage;
use File;
use View;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;

use Omnipay\Omnipay;
use Omnipay\Common\CreditCard;

use GuzzleHttp\Client;
use Carbon\Carbon;

class APICompanyController extends Controller
{
    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    public function register(Request $request)
    {

    }

    public function log_in(Request $request)
    {
        $company = Company::where('email', $request['email'])->first();

        if(!isset($company)){
            return 'error';
        }

        if (Hash::check($request['password'], $company->password)) {
            return response()->json([
                                    'user'   => $company,
                                    'status' => 'success'

                                    ]);
        } else {
            return redirect()->back();
        }
    }

    public function notifications()
    {
        $notifications = DB::table('notifications')->where('user_id', 1)->where('is_company',0)->get();
        return Response::json($notifications);
    }

    public function getDashboard($id)
    {
        $reports  = Report::where('company_id', $id)->count();
        $billing  = BillingInformation::where('company_id', $id)->first();
        $low      = Report::where('company_id', $id)->where('impact', 'Low')->count();
        $medium   = Report::where('company_id', $id)->where('impact', 'Medium')->count();
        $critical = Report::where('company_id', $id)->where('impact', 'Critical')->count();
        $hackers = Report::where('company_id', $id)
        ->groupBy('hacker_id')
        ->count();
        $vulns   = Report::distinct()->where('company_id', $id)->orderBy('created_at', 'desc')->groupBy('vulnerability_id')->LIMIT(3)->get();

        $vulnerabilities = array();
        foreach ($vulns as $vuln) {
            $vulnerabilities [] =  $vuln->vulnerability['name'];
        }
        return Response::json([
                              'low'               => $low / $reports * 100,
                              'totalLow'          => $low,
                              'medium'            => $medium / $reports * 100,
                              'totalMedium'       => $medium,
                              'critical'          => $critical / $reports * 100,
                              'totalCritical'     => $critical,
                              'reports'           => $reports,
                              'funds'             => number_format($billing->bounties_cost,2),
                              'hackers'           => $hackers,
                              'vulnerabilities'   => $vulnerabilities,
                              ]);
    }

    public function getReports($id)
    {
        $reports = Report::where('company_id', $id)->get();

        return Response::json($reports);
    }

    public function getReportDetails($id)
    {
        $report =  Report::has('hacker')->find($id);

        return response()->json($report);
    }

    public function getSettings($id)
    {
        $profile = CompanyProfile::find($id);

        return response()->json([
                                'profile' => $profile,
                                ]);
    }

    public function changeProgram($id, $status)
    {
        CompanyProfile::where('id', $id)->update(['is_public' => $status]);

        return response()->json([
                                'profile' => CompanyProfile::find($id),
                                ]);
    }
}