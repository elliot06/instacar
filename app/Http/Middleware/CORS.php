<?php

namespace App\Http\Middleware;

use Closure;

class CORS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // header('Access-Control-Allow-Origin: *');
        // header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        // header('OPTIONS, PUT, DELETE',
        //     'Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization, X-Requested-With');
        // header('Access-Control-Allow-Credentials: true');

        // $headers = [
        //     'Access-Control-Allow-Origin' => '*',
        //     'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
        //     'Access-Control-Allow-Headers' =>  'Content-Type, X-Auth-Token, Origin, Authorization, X-Requested-With'
        // ];

        // if(!$request->isMethod("options")) {
        //     // return Response::make('OK', 200, $headers);
        //     return $next($request);

        // }

        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
        // $response = $next($request);

        // foreach ($headers as $key => $value) 
        //     $response->header($key, $value);
        
        // return $response;
    }
}
