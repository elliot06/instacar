<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::group(['middleware' => ['api', 'cors'],'prefix' => 'api'], function() {

	Route::post('/company/log_in/', 'APICompanyController@log_in');
	Route::post('/company/register/', 'APICompanyController@register');
	Route::get('company/notifications', 'APICompanyController@notifications');
	Route::get('/company/dashboard/{id}', 'APICompanyController@getDashboard');
	Route::get('/company/reports/{id}', 'APICompanyController@getReports');
	Route::get('/company/report/details/{id}', 'APICompanyController@getReportDetails');
	Route::get('/company/settings/{id}', 'APICompanyController@getSettings');
	Route::post('/company/change/program/{id}/{status}', 'APICompanyController@changeProgram');
});
