<?php namespace App;
use Illuminate\Database\Eloquent\Model;
class CompanyPolicy extends Model
{
    protected $table = 'company_policy';
    protected $dates = ['created_at', 'updated_at'];
    public function company()
    {
      return $this->belongsTo('App\Company');
    }
    public function bounty()
    {
        return $this->hasMany('App\CompanyBounty','policy_id');
    }
    public function domain()
    {
        return $this->hasOne('App\CompanyDomain','policy_id');
    }
}
