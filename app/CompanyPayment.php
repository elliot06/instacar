<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPayment extends Model
{
    protected $table = 'company_payments';
     protected $dates = ['created_at', 'updated_at', 'next_payment_in'];

    public function company()
    {
      return $this->belongsTo('App\Company');
    }
}
