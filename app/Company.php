<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
use Auth;


class Company extends Model implements AuthenticatableContract,
                                    CanResetPasswordContract {
    use Authenticatable, CanResetPassword;


    protected $table = 'perma_company_acc';

   public function report()
    {
    	return $this->hasMany('App\Report');
    }

    public function profile()
    {
    	return $this->hasOne('App\CompanyProfile');
    }

    public function company_payment()
    {
      return $this->hasMany('App\CompanyPayment');
    }


    public function hacker_bounties()
    {
        return $this->hasMany('App\HackerBounties');
    }

    public function getPublicPrograms()
    {
     
      $users = DB::table('company_payments')
            ->join('perma_company_acc','perma_company_acc.id','=','company_payments.company_id')
            ->join('company_profile', 'company_payments.company_id', '=', 'company_profile.company_id')
            ->join('billing_information','perma_company_acc.id','=','billing_information.company_id') 
            ->select('perma_company_acc.*', 'company_profile.logo_pic','billing_information.package_id')
            ->where('company_profile.is_public',1)
            ->wherein('company_payments.created_at',function($query)
            {
                $query->select(DB::raw('MAX(company_payments.created_at)'))
                      ->from('company_payments')
                      ->groupBy('company_payments.company_id');
            })
            
            ->get();
        if($users !== null){
        	return $users;
    	}else{
    		return null;
    	}
    }

    public function policy()
    {
        return $this->hasOne('App\CompanyPolicy');
    }

    public static function getCompanyPackageName($id)
    {
        $package_name = DB::table('billing_information')
                      ->where('company_id',$id)
                      ->join('packages','billing_information.package_id','=','packages.id')
                      ->select('packages.package_name')
                      ->first();

            return $package_name;
    }

    public function domain()
    {
        return $this->hasOne('App\CompanyDomain');
    }

    public function billing()
    {
        return $this->hasOne('App\BillingInformation');
    }

    public function companyImpactRange()
    {
        return $this->hasMany('App\CompanyImpactRange','company_id');
    }
}
