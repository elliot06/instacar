<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model
{
    //
	protected $table = 'company_profile';
    protected $dates = ['billing_starts_at'];
    
    public function company()
    {
    	return $this->belongsTo('App\Company');
    }

    public function report()
    {
    	return $this->belongsTo('App\Report');
    }


}
