<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Report extends Model
{
    //
    protected $table = 'reports';
    protected $dates = ['disclosed_at','resolved_at'];

    public function hacker()
    {
        return $this->belongsTo('App\Hacker');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function message()
    {
        return $this->hasMany('App\Message');
    }

    public function reportImage()
    {
        return $this->hasMany('App\ReportImage');
    }

    public function vulnerability()
    {
        return $this->hasOne('App\Vulnerability','id','vulnerability_id');
    }
    public function package()
    {
        return $this->hasOne('App\BillingInformation','id','package_id');
    }

    public function participant()
    {
        return $this->belongsTo('App\Participant','id','report_id');
    }

    public function paypal()
    {
        return $this->hasOne('App\Paypal');
    }


    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
    public function hacker_bounties()
    {
        return $this->hasOne('App\HackerBounties','report_id');
    }

    public function addParticipant($userId,$isAdmin)
    {
        Participant::firstOrCreate([
            'user_id' => $userId,
            'is_admin' => $isAdmin,
            'report_id' => $this->id,
        ]);
    }

    public function getParticipantFromUser($userId,$isAdmin)
    {
        return DB::table('participants')->where(['user_id' => $userId , 'is_admin' => $isAdmin])->first();
    }

    public function markAsRead($userId,$isAdmin)
    {
        try {
            //$participant = $this->getParticipantFromUser($userId,$isAdmin);
            $participant = DB::table('participants')->where(['user_id' => $userId , 'is_admin' => $isAdmin])
                ->update(['last_read' => \Carbon\Carbon::now()]);
        } catch (ModelNotFoundException $e) {
            //do nothing
        }
    }

    public function participantsUserIds($userId = null,$isAdmin = 0 ,$id = null)
    {
        $users = Participant::withTrashed()->select('user_id')->where(['user_id' => $userId ,'is_admin' => $isAdmin ,'report_id' => $id])->first();

        if ($users != null) {
            return $users;
        }else{
            $users = Participant::withTrashed()->select('user_id')->where(['is_admin' => $isAdmin ,'report_id' => $id])->first();
            return $users;
        }


    }

    public static function getAllLatest()
    {
        return self::latest('updated_at');
    }

    public function scopeForUserWithNewMessages($query, $userId , $isAdmin)
    {
        $participantTable = 'participants';
        $threadsTable = 'reports';

        return $query->join($participantTable, $this->getQualifiedKeyName(), '=', $participantTable . '.report_id')
            ->where([$participantTable . '.user_id' => $userId , $participantTable .'.is_admin' => $isAdmin ])
            ->whereNull($participantTable . '.deleted_at')
            ->where(function ($query) use ($participantTable, $threadsTable) {
                $query->where($threadsTable . '.updated_at', '>', $this->getConnection()->raw($this->getConnection()->getTablePrefix() . $participantTable . '.last_read'))
                    ->orWhereNull($participantTable . '.last_read');
            })
            ->select($threadsTable . '.*');
    }

    public function scopeForUser($query, $userId ,$isAdmin)
    {
        $participantsTable = 'participants';
        $threadsTable = 'reports';

        return $query->join($participantsTable, $this->getQualifiedKeyName(), '=', $participantsTable . '.report_id')
            ->where($participantsTable . '.user_id', $userId)
            ->where($participantsTable . '.is_admin', $isAdmin)
            ->select($threadsTable . '.*');
    }

    public function isUnread($userId,$isAdmin)
    {
        try {
            $participant = $this->getParticipantFromUser($userId,$isAdmin);

            if ($participant->last_read === null || $this->updated_at->gt($participant->last_read)) {
                return true;
            }
        } catch (ModelNotFoundException $e) {
            // do nothing
        }

        return false;
    }

    public function report_comment()
    {
        return $this->hasMany('App\ReportComment');
    }

    public static function reports_not_in_package($company_not_package)
    {
        $totalrepnotpackage = DB::table('reports')
            ->select('packages.id')
            ->join('vulnerability_types', 'reports.vulnerability_id', '=', 'vulnerability_types.id')
            ->join('packages', 'vulnerability_types.package_id', '=', 'packages.id')
            ->where('packages.id','<',$company_not_package)
            ->groupBy('packages.id')
            ->count('packages.id');

        return $totalrepnotpackage;
    }

    public static function hackers_not_in_package($company_not_package)
    {
        $hackerrepnotpackage=DB::table('reports')
            ->select('perma_hacker_acc.username')
            ->join('perma_hacker_acc','reports.hacker_id','=','perma_hacker_acc.id')
            ->join('vulnerability_types', 'reports.vulnerability_id', '=', 'vulnerability_types.id')
            ->join('packages', 'vulnerability_types.package_id', '=', 'packages.id')
            ->groupBy('perma_hacker_acc.username')
            ->where('packages.id','!=',$company_not_package)
            ->get();

        return $hackerrepnotpackage;
    }
}
