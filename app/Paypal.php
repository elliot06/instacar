<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paypal extends Model
{
	//relations to send bounties  per report

   	protected $table = 'hacker_paypal';

    public function hacker()
    {
    	return $this->belongsTo('App\Hacker');
    }
    public function report()
    {
        return $this->belongsTo('App\Report');
    }
}
